DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS pupils;

CREATE TABLE classes(
	class_id INTEGER AUTO_INCREMENT,
	class_name VARCHAR(128),
	instructor VARCHAR(128),
	days VARCHAR(8),
	start VARCHAR(8),
	duration INTEGER,
	PRIMARY KEY(class_id)
);

INSERT INTO classes(class_name, instructor, days, start, duration) 
VALUES
 ('Adults Advanced', 'Mr. Ndiba', 'T/TH', '18:45', 50)
,('Kids Advanced', 'Mr. Ndiba', 'T/TH', '17:45', 50)
,('Adults Beginner', 'Mr. Harbor', 'M/W/F', '18:45', 50)
,('Kids Beginner', 'Mr. Harbor', 'M/W/F', '17:45', 50);

CREATE TABLE pupils (
	tx_id INTEGER AUTO_INCREMENT,
	class_id INTEGER NOT NULL,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	enroll_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(tx_id)
);

INSERT INTO pupils (class_id,first_name,last_name)
VALUES
 (1, 'Jeffery', 'Brannon')
,(1, 'Joseph', 'Taylor')
,(2, 'Robert', 'Smith')
,(2, 'Daylan', 'Arnold')
,(3, 'Tim', 'Waylan')
,(3, 'Steve', 'Arnold')
,(4, 'Angie', 'Martin')
,(4, 'Annette', 'Martin');

