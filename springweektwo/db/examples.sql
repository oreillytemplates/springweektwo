DROP TABLE IF EXISTS examples;

CREATE TABLE examples (
	id INTEGER AUTO_INCREMENT,
	first_name VARCHAR(32) NOT NULL,
	last_name VARCHAR(32) NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO examples (first_name, last_name)
VALUES
 ('Jeffery', 'Brannon')
,('Nathan', 'Sutton')
,('Beth', 'Coleman');