package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.examples.Example;

public interface ExampleService {
    public List<Example> getExamples();
    public List<Example> getExampleById(Integer id);
	public void testQueries(String day);
}
