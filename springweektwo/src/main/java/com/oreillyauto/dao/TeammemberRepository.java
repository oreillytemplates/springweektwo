package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.TeammemberRepositoryCustom;
import com.oreillyauto.domain.facilities.Teammember;

public interface TeammemberRepository extends CrudRepository<Teammember, Integer>, TeammemberRepositoryCustom {
    
}
