package com.oreillyauto.dao.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.UserRepositoryCustom;
import com.oreillyauto.domain.User;

@Repository
public class UserRepositoryImpl extends QuerydslRepositorySupport implements UserRepositoryCustom {
	
    public UserRepositoryImpl() {
        super(User.class);
    }
    
}

