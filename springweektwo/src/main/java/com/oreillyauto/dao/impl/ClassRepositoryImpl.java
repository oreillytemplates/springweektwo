package com.oreillyauto.dao.impl;

import java.util.Map;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.ClassRepositoryCustom;
import com.oreillyauto.domain.foo.Clazz;
//import com.oreillyauto.domain.foo.QClazz;
//import com.querydsl.core.group.GroupBy;

@Repository
public class ClassRepositoryImpl extends QuerydslRepositorySupport implements ClassRepositoryCustom {
	
//	private QClazz classTable = QClazz.clazz;
	
    public ClassRepositoryImpl() {
        super(Clazz.class);
    }

    @Override
    @SuppressWarnings("unchecked")
	public Map<String,Clazz> getClasses() {
//		Map<String, Clazz> classMap = (Map<String, Clazz>) (Object) getQuerydsl().createQuery()
//                .from(classTable)
//                .orderBy(classTable.className.asc())
//                .transform(GroupBy.groupBy(classTable.className) // Map Key
//                		.as(classTable));                        // Map Value
//        return classMap;
    	return null;
	}
    
}

