package com.oreillyauto.domain.schools;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STUDENTS")
public class Student implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Student() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id", columnDefinition = "INTEGER")
    private Integer studentId;
    
    @Column(name = "school_id", columnDefinition = "INTEGER")
    private Integer schoolId;
    
    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;
    
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String lastName;
    
	public Integer getStudentId() {
		return studentId;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Teammember [studentId=" + studentId + ", schoolId=" + schoolId + ", firstName=" + firstName + ", lastName="
				+ lastName + "]";
	}

}
