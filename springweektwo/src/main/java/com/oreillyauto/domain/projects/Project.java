package com.oreillyauto.domain.projects;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PROJECTS")
public class Project { 
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id", columnDefinition = "INTEGER")
    private Integer projectId;
	
    @Column(name = "title", columnDefinition = "VARCHAR(64)")
    private String title;

    @ManyToMany(mappedBy = "projects", fetch = FetchType.EAGER)
    private Set<Planner> employees = new HashSet<Planner>();
    
	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Planner> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Planner> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", title=" + title + "]";
	}
    
}