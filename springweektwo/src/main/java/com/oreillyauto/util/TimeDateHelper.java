package com.oreillyauto.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeDateHelper {

	public static Timestamp convertOrlyDatepickerToTimestamp(String dateStr) throws Exception {
		DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = utcFormat.parse(dateStr);
		DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		pstFormat.setTimeZone(TimeZone.getTimeZone(getCurrentTimezone())); 
		return getTimestampByString(pstFormat.format(date), "yyyy-MM-dd'T'HH:mm:ss.SSS");
	}
	
	public static Timestamp getTimestampByString(String dateString, String currentFormat) {
		try {
			Date date = new SimpleDateFormat(currentFormat).parse(dateString);
			return new Timestamp(date.getTime());
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static String getCurrentTimezone() {
		Calendar c = Calendar.getInstance();
        return c.getTimeZone().getDisplayName();
	}

}
